package com.domain.Prometheus;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import org.springframework.boot.actuate.endpoint.AbstractEndpoint;
import com.codahale.metrics.MetricRegistry;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.dropwizard.DropwizardExports;
import io.prometheus.client.exporter.common.TextFormat;

public class PrometheusEndpoint extends AbstractEndpoint<String>{

  private CollectorRegistry registry;
  private MetricRegistry dropWizardRegistry;

  public PrometheusEndpoint(CollectorRegistry registry, MetricRegistry dropWizardRegistry){
    super("prometheus", false, true);
    this.registry = registry;
    this.dropWizardRegistry=dropWizardRegistry;
    this.registry.register(new DropwizardExports(this.dropWizardRegistry));
  }

  @Override
  public String invoke(){
    Writer writer = new StringWriter();
    try{
      TextFormat.write004(writer, registry.metricFamilySamples());
    }
    catch(IOException e){
      e.printStackTrace();
    }
    return writer.toString();
  }

}
