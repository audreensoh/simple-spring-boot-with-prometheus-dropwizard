package com.domain.Prometheus;

import org.springframework.boot.actuate.autoconfigure.ManagementContextConfiguration;
import org.springframework.boot.actuate.condition.ConditionalOnEnabledEndpoint;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;

import com.codahale.metrics.MetricRegistry;

import io.prometheus.client.CollectorRegistry;

@ManagementContextConfiguration
public class PrometheusEndpointContextConfiguration {
    
    @Bean
    PrometheusEndpoint prometheusEndpoint(CollectorRegistry registry, MetricRegistry dropWizardRegistry) {
        return new PrometheusEndpoint(registry,dropWizardRegistry);
    }
 
    @Bean
    @ConditionalOnBean(PrometheusEndpoint.class)
    @ConditionalOnEnabledEndpoint("prometheus")
    PrometheusMvcEndpoint prometheusMvcEndpoint(PrometheusEndpoint prometheusEndpoint) {
        return new PrometheusMvcEndpoint(prometheusEndpoint);
    }
 
    @Bean
    CollectorRegistry collectorRegistry() {
        return new CollectorRegistry();
    }

}