package com.domain.Entity;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Document
public class Student{
  
  @Id
  private int id;
  private String name;
  private String course;
  private Collection<Address> address;
  
  public Student(){
    
  }


public Student(int id, String name, String course){
    this.id = id;
    this.name = name;
    this.course = course;
  }


public Student(int id, String name, String course, Collection<Address> address){
  super();
  this.id = id;
  this.name = name;
  this.course = course;
  this.address = address;
}


public Collection<Address> getAddress(){
  return address;
}


public void setAddress(Collection<Address> address){
  this.address = address;
}

public void addAddress(Address address){
  this.address.add(address);
}

public int getId(){
  return id;
}


public String getName(){
  return name;
}


public String getCourse(){
  return course;
}


public void setId(int id){
  this.id = id;
}


public void setName(String name){
  this.name = name;
}


public void setCourse(String course){
  this.course = course;
}


@Override
public String toString(){
  return "Student [id=" + id + ", name=" + name + ", course=" + course + ", address=" + address + "]";
}



}
