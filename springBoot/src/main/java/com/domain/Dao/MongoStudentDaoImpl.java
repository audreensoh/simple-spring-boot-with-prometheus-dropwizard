package com.domain.Dao;



import java.awt.List;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.domain.Entity.Address;
import com.domain.Entity.Student;

@Repository
@Qualifier("mongoData")
public class MongoStudentDaoImpl implements StudentDao {

  MongoTemplate mongo;
  String COLLECTION_NAME="Student";
  
  @Autowired
  public MongoStudentDaoImpl(MongoTemplate  mongoDb){
    this.mongo=mongoDb;
    
//    Address a= new Address(1,"Blood Stoney Road","Dublin","Dublin","00000");
//    ArrayList<Address> c= new ArrayList<Address>();
//    c.add(a);
    
//    mongo.save(new Student(1, "Said", "Computer Science",c));
//    mongo.insert(new Student(2, "Alex U", "Finance"));
//    mongo.insert(new Student(3, "Ann", "Maths"));
    
    
//    {
//      "id": 2,
//      "name": "Alex U",
//      "course": "Finance",
//      "address": [
//        {
//          "id": 1,
//          "street": "Parnel Street",
//          "city": "Dublin",
//          "state": "Dublin",
//          "zip": "00000"
//        }
//      ]
//    }
    
    System.out.println("Setting up Mongo");
}
  
  public Collection<Student> getAllStudents(){

    return this.mongo.findAll(Student.class);
  }

  public Student getStudentById(int id){
    Query query = new Query(Criteria.where("id").is(id));
    
    return this.mongo.findOne(query, Student.class);
  }

  public void deleteStudentById(int id){
    this.mongo.remove(new Query(Criteria.where("id").is(id)), Student.class);
  }

  public void updateStudent(Student student){
    System.out.println(student);
    this.mongo.save(student);
  }

  public void insertStudentToDb(Student student){
    this.mongo.insert(student);
  }
  
  

}
