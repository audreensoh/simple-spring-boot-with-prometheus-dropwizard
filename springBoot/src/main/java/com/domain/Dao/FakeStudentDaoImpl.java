package com.domain.Dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.domain.Entity.Student;

@Repository
@Qualifier("fakeData")
public class FakeStudentDaoImpl implements StudentDao{
  private static Map<Integer,Student> students;

  static{
    students = new HashMap<Integer,Student>(){
      {
        put(1, new Student(1, "Said", "Computer Science"));
        put(2, new Student(2, "Alex U", "Finance"));
        put(3, new Student(3, "Ann", "Maths"));
      }
    };
  }

  /* (non-Javadoc)
   * @see com.domain.Dao.StudentDao#getAllStudents()
   */
  public Collection<Student> getAllStudents(){
    System.out.println("Getting Students from FakeData");
    System.out.println(this.students.size());


    return this.students.values();
  }

  /* (non-Javadoc)
   * @see com.domain.Dao.StudentDao#getStudentById(int)
   */
  public Student getStudentById(int id)
  {
    return this.students.get(id);
  }

  /* (non-Javadoc)
   * @see com.domain.Dao.StudentDao#deleteStudentById(int)
   */
  public void deleteStudentById(int id){
    this.students.remove(id);
    
  }
  
  /* (non-Javadoc)
   * @see com.domain.Dao.StudentDao#updateStudent(com.domain.Entity.Student)
   */
  public void updateStudent(Student student)
  {
    Student s= students.get(student.getId());
    s.setCourse(student.getCourse());
    s.setName(student.getName());
    students.put(student.getId(), student);
    
  }

  /* (non-Javadoc)
   * @see com.domain.Dao.StudentDao#insertStudentToDb(com.domain.Entity.Student)
   */
  public void insertStudentToDb(Student student){
    this.students.put(student.getId(),student);
    
  }
}


