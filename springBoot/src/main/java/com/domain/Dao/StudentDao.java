package com.domain.Dao;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.domain.Entity.Student;


public interface StudentDao{

  Collection<Student> getAllStudents();

  Student getStudentById(int id);

  void deleteStudentById(int id);

  void updateStudent(Student student);

  void insertStudentToDb(Student student);

}
