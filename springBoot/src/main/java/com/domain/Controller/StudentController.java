package com.domain.Controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.domain.Entity.Student;
import com.domain.Service.StudentService;

@RestController
@RequestMapping("/students")
public class StudentController{
  @Autowired
  private StudentService studentService;

  @RequestMapping(method = RequestMethod.GET)
  @Timed
  public Collection<Student> getAllStudents(){
    return studentService.getAllStudents();
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  @Timed
  public Student getStudentById(@PathVariable("id") int id){
    System.out.println(this.studentService.getStudentById(id).toString());
    return this.studentService.getStudentById(id);
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  @Timed
  public void deleteStudentById(@PathVariable("id") int id){
    this.studentService.deleteStudentById(id);
  }

  //Remember to change input type to JSON 
  @RequestMapping(method = RequestMethod.PUT, consumes= MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public void updateStudent(@RequestBody Student student)
  {
    this.studentService.updateStudent(student);
  }
  
  @RequestMapping(method = RequestMethod.POST, consumes= MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public void addStudent(@RequestBody Student student){
    this.studentService.insertStudent(student);
  }
}
