package com.domain.Service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.domain.Dao.StudentDao;
import com.domain.Entity.Student;

@Service
public class StudentService{
  @Autowired
  @Qualifier("mongoData")
  private StudentDao studentDAO;

  public Collection<Student> getAllStudents(){
    return studentDAO.getAllStudents();
  }
  
  public Student getStudentById(int id)
  {
    return this.studentDAO.getStudentById(id);
  }

  public void deleteStudentById(int id){
    //validation can be added here
    this.studentDAO.deleteStudentById(id);    
  }

  public void updateStudent(Student student)
  {
    this.studentDAO.updateStudent(student);
  }

  public void insertStudent(Student student){
   
    this.studentDAO.insertStudentToDb(student);
  }

}
